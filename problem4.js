//Q4 Group users based on their Programming language mentioned in their 
function groupUserLanguage(users){
    let userAndDesgination = {};
    for(let key in users){
        const designationPresent = users[key].desgination;
        if (!(designationPresent in userAndDesgination)) {
            userAndDesgination[designationPresent] = [key];
        } else {
            userAndDesgination[designationPresent].push(key);
        }
    }
    return userAndDesgination;
}

module.exports = groupUserLanguage;


